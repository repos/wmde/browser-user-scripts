# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.2] - 2024-06-12

### Changed

- Removed notification count (the "(1)" prefix) from the copied phabricator title as well

## [1.1.1] - 2024-06-10

### Changed

- Removed the `⚓️ ` from phabricator links

## [1.1.0] - 2023-03-07

### Added

- Button to copy a Phabricator link formatted as internal interwiki link to phabricator
